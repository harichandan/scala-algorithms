package hari.source

import scala.annotation.tailrec

object FactorialTailRec {
  def factorial(x: Int) = {
    @tailrec
    def factHelper(x: Int, result: Int = 1): Int = {
      if (x == 0) result else factHelper(x - 1, result * x)
    }

    factHelper(x)
  }
  def main(args: Array[String]) {
    println(factorial(5))
  }
}