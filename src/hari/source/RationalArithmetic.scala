package hari.source

object RationalArithmetic {
  def main(args: Array[String]) {
    val one = new Rational(3, 4)
    val two = new Rational(2, 3)
    println(one + two)
    println(one - two)
    println(one * two)
    println(one / two)
    println(-one)
    println(one < two)
    println(one max two)
  }
}

class Rational(x: Int, y: Int) {
  require(y != 0, "Denominator should not be zero")
  private def gcd(a: Int, b: Int): Int = if (b == 0) a else gcd(b, a % b)
  def num = x / gcd(x, y)
  def den = y / gcd(x, y)

  override def toString = num + "/" + den

  def <(that: Rational) = {
    this.num * that.den < that.num * that.den
  }

  def max(that: Rational) = {
    if (this < that) that else this
  }

  def +(rational: Rational) = {
    new Rational(
      num * rational.den + rational.num * den,
      den * rational.den)
  }

  def unary_- = new Rational(-num, den)

  def -(that: Rational) = {
    this + -that
  }

  def *(rational: Rational) = {
    new Rational(num * rational.num, den * rational.den)
  }

  def inv = new Rational(den, num)

  def /(that: Rational) = {
    this * that.inv
  }
}