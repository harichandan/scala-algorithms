

package hari.source

object IsPrime {
  def main(args: Array[String]) {
    (2 to 100).map(x => println(x, isPrime(x)))
  }
  
  def isPrime(n: Int) = (2 until n) forall (x => n%x != 0)
}