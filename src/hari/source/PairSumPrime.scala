package hari.source

object PairSumPrime {
  def main(args: Array[String]) {
    pairSumPrime(5).map(println)
  }
  
  def pairSumPrime(n: Int) = {
    for {
      i <- 1 until n
      j <- 1 until i
      if IsPrime.isPrime(i + j)
    } yield (i,j)
  }
}