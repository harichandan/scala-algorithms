package hari.source

import scala.annotation.tailrec

object Fibonacci {
  def main(args: Array[String]) {
    println(fibonacci(4));
  }

  def fibonacci(n: Int): Int = {
    @tailrec
    def fib_helper(n: Int, a: Int, b: Int): Int = {
      n match {
        case 0 => a
        case _ => fib_helper(n - 1, b, b + a)
      }
    }
    return fib_helper(n, 0, 1);
  }
}