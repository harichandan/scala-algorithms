// Given a string, print whether it's possible to 
// build a palindrome from it

package hari.source

object CanBuildPalindrome {
  def main(args: Array[String]): Unit = {
    println(canBuildPalindrome("liril"))
    println(canBuildPalindrome("hari"))
  }

  def canBuildPalindrome(str: String): Boolean = {
    str.toList.distinct.map(char => str.count(_ == char)).filter(_ % 2 == 1).size <= 1
  }
}